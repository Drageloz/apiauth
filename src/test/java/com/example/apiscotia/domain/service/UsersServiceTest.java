package com.example.apiscotia.domain.service;

import com.example.apiscotia.domain.dto.Users;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class UsersServiceTest {

    /*
    [3/31 4:24 PM] Rodriguez Rodriguez, William

    Consulta de información personal
    CÓDIGO HU1: 001
    NOMBRE HU: Consulta Info
    DESCRIPCIÓN HU: Yo como cliente de Scotiabank-Colpatria quiero acceder
    a mi información personal utilizando mi ID personal de customer de manera
    segura para utilizar los servicios a los que tengo acceso como cliente.
    Criterio de Aceptación:

    1. Dado que abrí la aplicación, consumir un servicio donde puede obtener y
    confirmar mis datos personales.

    2. Dado que ingresé mi customer ID, espero que el servicio muestre toda mi data personal.

    3. Dado una combinación de customer ID erróneo, cuando intente acceder al aplicativo,
    se mostrará un mensaje de error.
     */
    @Autowired
    UsersService usersService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getAll() {
        List<Users> user = usersService.getAll();
        assertThat(Arrays.asList(new Users(
                        1,
                        "andres",
                        "suarez",
                        "a@gmail.com",
                        "male",
                        "192.168.1.1")),
                is(equalTo(user)));
    }

    @Test
    void getUser() {
        Users user = usersService.getUser(1).get();
        assertThat(new Users(
                        1,
                        "andres",
                        "suarez",
                        "a@gmail.com",
                        "male",
                        "192.168.1.1"),
                is(equalTo(user)));
    }

    @Test
    void getUserByUserName() {
        Users user = usersService.getUserByUserName("andres").get();
        assertThat(new Users(
                        1,
                        "andres",
                        "suarez",
                        "a@gmail.com",
                        "male",
                        "192.168.1.1"),
                is(equalTo(user)));
    }

    @Test
    void getUserByLastName() {
        Users user = usersService.getUserByLastName("suarez").get();
        assertThat(new Users(
                1,
                "andres",
                "suarez",
                "a@gmail.com",
                "male",
                "192.168.1.1"),
                is(equalTo(user)));

    }
}