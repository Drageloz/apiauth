package com.example.apiscotia.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class EntityUsers {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "us_id")
    private Integer usId;


    @Column(name = "us_first_name")
    private String usUser;

    @Column(name = "us_last_name")
    private String usLastName;

    @Column(name = "us_email")
    private String usEmail;

    @Column(name = "us_gender")
    private String usGender;

    @Column(name = "us_ip_address")
    private String usIpAddress;

    public Integer getUsId() {
        return usId;
    }

    public void setUsId(Integer usId) {
        this.usId = usId;
    }

    public String getUsUser() {
        return usUser;
    }

    public void setUsUser(String usUser) {
        this.usUser = usUser;
    }

    public String getUsLastName() {
        return usLastName;
    }

    public void setUsLastName(String usLastName) {
        this.usLastName = usLastName;
    }

    public String getUsEmail() {
        return usEmail;
    }

    public void setUsEmail(String usEmail) {
        this.usEmail = usEmail;
    }

    public String getUsGender() {
        return usGender;
    }

    public void setUsGender(String usGender) {
        this.usGender = usGender;
    }

    public String getUsIpAddress() {
        return usIpAddress;
    }

    public void setUsIpAddress(String usIpAddress) {
        this.usIpAddress = usIpAddress;
    }
}
