package com.example.apiscotia.persistence.mapper;

import com.example.apiscotia.domain.dto.Users;
import com.example.apiscotia.persistence.entity.EntityUsers;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsersMapper{

    @Mappings({
            @Mapping(source = "usId", target = "id"),
            @Mapping(source = "usUser", target = "user"),
            @Mapping(source = "usLastName", target = "lastName"),
            @Mapping(source = "usEmail", target = "email"),
            @Mapping(source = "usGender", target = "gender"),
            @Mapping(source = "usIpAddress", target = "ipAddress"),
    })
    Users toUsers(EntityUsers entityUsers);
    List<Users> toUsers(List<EntityUsers> entityUsers);

}
