package com.example.apiscotia.persistence.crud;

import com.example.apiscotia.persistence.entity.EntityUsers;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UsersCrudRepository extends CrudRepository<EntityUsers, Integer> {

    Optional<EntityUsers> findByUsUser(String userName);
    Optional<EntityUsers> findByUsLastName(String userName);
}