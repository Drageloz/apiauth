package com.example.apiscotia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiscotiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiscotiaApplication.class, args);
	}

}
