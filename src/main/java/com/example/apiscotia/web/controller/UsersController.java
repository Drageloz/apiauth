package com.example.apiscotia.web.controller;


import com.example.apiscotia.domain.dto.Users;
import com.example.apiscotia.domain.dto.UsersAccessID;
import com.example.apiscotia.domain.service.UsersService;
import com.example.apiscotia.web.exception.ApiRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Users")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST}, allowedHeaders = "*")
public class UsersController {

    @Autowired
    ApiRequestException apiRequestException;

    @Autowired
    UsersService usersService;

    @GetMapping("/accessWithId/{id}")
    public ResponseEntity<UsersAccessID> accessWithId(@PathVariable("id") Integer id){
//        return usersService.accessWithId(id)
//                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
//                .orElseThrow(ApiRequestException::new)
        Optional<UsersAccessID> usersAccessID = usersService.accessWithId(id);
        if(usersAccessID.isPresent()){
            return new ResponseEntity<>(usersAccessID.get(), HttpStatus.OK);
        }
        else{
            throw new ApiRequestException("No se encontró el usuario con las credenciales otorgadas");
        }

    }

    @GetMapping("/all")
    public ResponseEntity<List<Users>> getAll(){
        return new ResponseEntity<>(usersService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<Users> getByUserName(@PathVariable("name") String userName){
        return usersService.getUserByUserName(userName)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/lastname/{lastName}")
    public ResponseEntity<Users> getByLastName(@PathVariable("lastName") String lastName){
        return usersService.getUserByLastName(lastName)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }



}
