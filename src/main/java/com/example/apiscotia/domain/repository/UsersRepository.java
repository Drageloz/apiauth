package com.example.apiscotia.domain.repository;

import com.example.apiscotia.domain.dto.Users;

import java.util.List;
import java.util.Optional;

public interface UsersRepository {

    List<Users> getAll();
    Optional<Users> getUser(int UserId);
    Optional<Users> getUserByUserName(String userName);
    Optional<Users> getUserByLastName(String lastName);

}