package com.example.apiscotia.domain.service;

import com.example.apiscotia.domain.dto.Users;
import com.example.apiscotia.domain.dto.UsersAccessID;
import com.example.apiscotia.domain.repository.UsersRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    public List<Users> getAll(){
        return usersRepository.getAll();
    }

    public Optional<Users> getUser(int userId){ return usersRepository.getUser(userId); }

    public Optional<Users> getUserByUserName(String userName){ return usersRepository.getUserByUserName(userName); }

    public Optional<Users> getUserByLastName(String lastName){ return usersRepository.getUserByLastName(lastName); }

    public Optional<UsersAccessID> accessWithId(Integer id){
        final String uri = "https://my.api.mockaroo.com/test-users?key=6cc75b90";
        ObjectMapper objectMapper = new ObjectMapper();

        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);

        try {
            List<UsersAccessID> items = objectMapper.readValue(
                    result,
                    objectMapper.getTypeFactory().constructParametricType(List.class, UsersAccessID.class)
            );

            Optional<UsersAccessID> user = Optional.ofNullable(items.stream()
                    .filter(usuario -> id.equals(usuario.getId()))
                    .findAny()
                    .orElse(null));

            return user;
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();


    }
}
