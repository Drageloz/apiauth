package com.example.apiscotia.domain.service;

import com.example.apiscotia.domain.dto.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ScotiaUserDetailsService implements UserDetailsService {


    @Autowired
    UsersService usersService;

    @Override
    public UserDetails loadUserByUsername(String lastname) throws UsernameNotFoundException {
        Users user = usersService.getUserByLastName(lastname)
                .map(userData -> userData)
                .orElse(null);
        return new User(user.getLastName(), "{noop}" + user.getId(), new ArrayList<>());
    }
}
